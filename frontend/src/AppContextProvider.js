import React from "react";
import useGet from "./hooks/useGet";
import axios from "axios";

const AppContext = React.createContext({
  todos: [],
  url: "",
});

function AppContextProvider({ children }) {
  // TODO Exercise Six: Get some todos from the backend.
  const { data: todos, url: url, reFetch: reFetch } = useGet("/api/todos", []);

  // The context value that will be supplied to any descendants of this component.
  const context = {
    todos: todos,
    url: url,
    reFetch: reFetch,
  };

  // Wraps the given child components in a Provider for the above context.
  return <AppContext.Provider value={context}>{children}</AppContext.Provider>;
}

export { AppContext, AppContextProvider };
