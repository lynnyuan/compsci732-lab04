import { AppContext } from "../AppContextProvider";
import { useContext } from "react";
import * as dayjs from "dayjs";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { useHistory, Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
    padding: theme.spacing(0, 3),
  },
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing(1)}px auto`,
    padding: theme.spacing(2),
  },
}));

function TodoList() {
  const history = useHistory();
  const classes = useStyles();
  const { todos, url, reFetch } = useContext(AppContext);
  console.log(todos);
  const now = dayjs();

  function printStatus(todo) {
    const due = dayjs(todo.dueDate);
    if (todo.isCompleted) {
      return "Completed";
    } else if (due.diff(now, "day") < 0 && todo.isCompleted === false) {
      return "Overdue";
    } else if (now.diff(due, "day") === 1 && todo.isCompleted === false) {
      return "Upcoming";
    } else {
      return "Incomplete";
    }
  }

  function handleDelete(id) {
    axios.delete(`${url}/${id}`);
    reFetch();
    history.replace(`/`);
  }

  if (todos && todos.length > 0) {
    return todos.map((todo, index) => (
      <div key={index} className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container wrap="nowrap" spacing={2}>
            <label>
              <Grid item>Title: {todo.title}</Grid>
              <Grid item>Description: {todo.description}</Grid>
              <Grid item>
                {" "}
                Created Date:
                {dayjs(todo.createdAt).format("DD/MM/YYYY")}
              </Grid>
              <Grid item>
                {" "}
                Due Date:
                {dayjs(todo.dueDate).format("DD/MM/YYYY")}
              </Grid>
              <Grid item>
                {" "}
                Status:
                {/* {todo.isCompleted ? "Completed" : "Not yet"} */}
                {printStatus(todo)}
              </Grid>
              <Grid item>
                <Link to={`/update/${todo._id}`}>
                  <Button variant="contained">Update</Button>
                </Link>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={() => handleDelete(todo._id)}
                >
                  Delete
                </Button>
              </Grid>
            </label>
          </Grid>
        </Paper>
      </div>
    ));
  } else {
    return <p>There are no to-do items!</p>;
  }
}

export default TodoList;
