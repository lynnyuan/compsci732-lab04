import { useState, useContext } from "react";
import Modal from "./Modal";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import { AppContext } from "./AppContextProvider";

var dayjs = require("dayjs");

function NewTodoDialog({ pageTitle }) {
  const { reFetch } = useContext(AppContext);
  const history = useHistory();
  const [newTodo, setNewTodo] = useState({
    title: "",
    description: "",
    dueDate: "",
  });

  const idToUpdate = useParams();

  function onCancelNewTodo() {
    history.goBack();
  }

  function onAddNewTodo() {
    axios.post(`/api/todos`, newTodo);
    reFetch();
    history.replace(`/`);
  }

  function onUpdateTodo(idToUpdate, newTodo) {
    axios.put(`api/todos/${idToUpdate.id}`, newTodo);
    reFetch();
    history.replace(`/`);
  }

  return (
    <Modal
      style={{
        width: "50%",
        height: "auto",
        background: "white",
        padding: "20px",
      }}
      dismissOnClickOutside={true}
      onCancel={onCancelNewTodo}
    >
      <form noValidate autoComplete="off">
        <h2>{pageTitle}</h2>
        <TextField
          id="title"
          label="Title"
          value={newTodo.title}
          onInput={(e) => {
            setNewTodo({ ...newTodo, title: e.target.value });
          }}
        />

        <TextField
          id="dueDate"
          label="Due Date"
          type="date"
          onChange={(e) =>
            setNewTodo({ ...newTodo, dueDate: dayjs(e.target.value) })
          }
          InputLabelProps={{
            shrink: true,
          }}
          required
        />

        <TextField
          id="description"
          label="Description"
          placeholder="Description"
          multiline
          value={newTodo.description}
          onInput={(e) => {
            setNewTodo({ ...newTodo, description: e.target.value });
          }}
        />
        <br />
        <br />
        {pageTitle === "Add a Todo" ? (
          <Button
            variant="contained"
            color="primary"
            onClick={() => onAddNewTodo(newTodo)}
          >
            Add
          </Button>
        ) : (
          <Button
            variant="contained"
            color="primary"
            onClick={() => onUpdateTodo(idToUpdate, newTodo)}
          >
            Update
          </Button>
        )}
      </form>
    </Modal>
  );
}

export default NewTodoDialog;
