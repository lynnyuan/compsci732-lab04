import TodoList from "./components/TodoList";
import { Switch } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Link, Route } from "react-router-dom";
import NewTodoDialog from "./NewTodoDialog";

function App() {
  return (
    <div>
      <h1>My Todo App!</h1>
      <Link to={`/add`}>
        <Button variant="contained" color="primary">
          {" "}
          Add
        </Button>
      </Link>
      <TodoList />
      <Switch>
        <Route path={`/add`}>
          <NewTodoDialog pageTitle={"Add a Todo"} />
        </Route>
        <Route path={`/update/:id`}>
          <NewTodoDialog pageTitle={"Update a Todo"} />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
