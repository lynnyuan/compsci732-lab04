import dummyjson from "dummy-json";
import fs from "fs";

const template = fs.readFileSync("./src/db/dummy-data.hbs", {
  encoding: "utf-8",
});
const dummyDataString = dummyjson.parse(template);
const dummyData = JSON.parse(dummyDataString);

export { dummyData };
