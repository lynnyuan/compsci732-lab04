/**
 * This program should be run in order to populate the database with dummy data for testing purposes.
 */

import mongoose from "mongoose";
import connectToDatabase from "./db-connect";
import { dummyData } from "./dummy-data";
import { createTodo, retrieveAllTodos, retrieveTodo } from "./todos-dao";
import { Todo } from "./todos-schema";

main();

async function main() {
  await connectToDatabase();
  console.log("Connected to database!");

  await clearDatabase();
  console.log();

  await addData();
  console.log();

  await createTodo(todo);

  await retrieveAllTodos();

  await retrieveTodo();

  // Disconnect when completes
  await mongoose.disconnect();
  console.log("Disconnected from database!");
}

// TODO Exercise Two: Complete the clearDatabase() and addData() functions below.

async function clearDatabase() {
  const todoDeleted = await Todo.deleteMany({});
  console.log(`removed ${todoDeleted.deletedCount}`);
}

async function addData() {
  for (let dummyTodo of dummyData.todos) {
    const dbTodo = new Todo(dummyTodo);
    await dbTodo.save();
  }
}
