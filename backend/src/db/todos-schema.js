import mongoose from "mongoose";

const Schema = mongoose.Schema;

// TODO Exercise One: Model your schema here. Make sure to export it!
const TodoSchema = new Schema(
  {
    title: { type: String, required: true },
    description: String,
    dueDate: { type: Date, required: true },
    isCompleted: { type: Boolean, reqiured: true },
  },
  {
    timestamps: {},
  }
);

export const Todo = mongoose.model("Todo", TodoSchema);
