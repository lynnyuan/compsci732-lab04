/**
 * This file contains functions which interact with MongoDB, via mongoose, to perform Todo-related
 * CRUD operations.
 */

import { dummyData } from "./dummy-data";
import { Todo } from "./todos-schema";

// TODO Exercise Three: Implement the five functions below.

async function createTodo(todo) {
  const newTodo = new Todo(todo);
  await newTodo.save();
  return newTodo;
}

async function retrieveAllTodos() {
  return await Todo.find();
}

async function retrieveTodo(id) {
  return await Todo.findById(id);
}

async function updateTodo(todo) {
  const todoToUpdate = await Todo.findById(todo._id);
  if (todoToUpdate) {
    todoToUpdate.title = todo.title;
    todoToUpdate.description = todo.description;
    todoToUpdate.isCompleted = todo.isCompleted;
    todoToUpdate.dueDate = todo.dueDate;
    await todoToUpdate.save();
    return true;
  }
}

async function deleteTodo(id) {
  await Todo.deleteOne({ _id: id });
}

export { createTodo, retrieveAllTodos, retrieveTodo, updateTodo, deleteTodo };
