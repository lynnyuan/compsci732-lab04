import routes from "../todos-routes";
import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import express from "express";
import axios from "axios";

let mongod, app, server;
let todo1, todo2, todo3;

beforeAll(async (done) => {
  mongod = new MongoMemoryServer();

  const connectionString = await mongod.getUri();
  await mongoose.connect(connectionString, { useNewUrlParser: true });

  app = express();
  app.use("/", routes);
  server = app.listen(3000, () => done());
});

beforeEach(async () => {
  const coll = await mongoose.connection.db.createCollection("todos");

  todo1 = {
    title: "Palmerston",
    description: "-90.572417",
    isCompleted: true,
  };

  todo2 = {
    title: "test2",
    description: "test2",
    isCompleted: false,
  };

  todo3 = {
    title: "Dundee",
    description: "71.165129",
    isCompleted: true,
  };
  await coll.insertMany([todo1, todo2, todo3]);
});

afterEach(async () => {
  await mongoose.connection.db.dropCollection("todos");
});

afterAll((done) => {
  server.close(async () => {
    await mongoose.disconnect();
    await mongod.stop();

    done();
  });
});

it("gets all todos from server", async () => {
  const response = await axios.get("http://localhost:3001/api/todos");
  const todos = response.data;

  expect(todos).toBeTruthy();
  expect(todos.length).toBe(10);

  expect(todos[0].title).toBe("Palmerston");
  expect(todos[0].description).toBe("-90.572417");
  expect(todos[0].isCompleted).toBe(true);
});

it("get a single todo from the server", async () => {
  const response = await axios.get(
    `http://localhost:3001/api/todos/${todo2._id}`
  );
  const todo = response.data;
  expect(todo.title).toBe("test2");
  expect(todo.description).toBe("test2");
  expect(todo.isCompleted).toBe(false);
});

it("get a single todo from the server", async () => {
  console.log(todo3);
  const response = await axios.get(
    `http://localhost:3001/api/todos/${todo3._id}`
  );
  const todo = response.data;
  expect(todo.title).toBe("Dundee");
  expect(todo.description).toBe("71.165129");
  expect(todo.isCompleted).toBe(true);
});
