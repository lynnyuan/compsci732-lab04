import express from "express";
import {
  createTodo,
  retrieveAllTodos,
  retrieveTodo,
  updateTodo,
  deleteTodo,
} from "../../db/todos-dao";

// const HTTP_OK = 200; // Not really needed; this is the default if you don't set something else.
const HTTP_CREATED = 201;
const HTTP_NOT_FOUND = 404;
const HTTP_NO_CONTENT = 204;

const router = express.Router();

// TODO Exercise Four: Add your RESTful routes here.

router.get("/", async (req, res) => {
  res.json(await retrieveAllTodos());
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  const todo = await retrieveTodo(id);

  if (todo) {
    res.json(todo);
  } else {
    res.sendStatus(HTTP_NOT_FOUND);
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const todo = req.body;
  todo._id = id;
  console.log(todo);
  const success = await updateTodo(todo);
  res.sendStatus(success ? HTTP_NO_CONTENT : HTTP_NOT_FOUND);
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  await deleteTodo(id);
  res.sendStatus(HTTP_NO_CONTENT);
});

router.post("/", async (req, res) => {
  const newTodo = await createTodo({
    title: req.body.title,
    description: req.body.description,
    dueDate: req.body.dueDate,
    isCompleted: false,
  });

  res
    .status(HTTP_CREATED)
    .header("Location", `/api/todos/${newTodo._id}`)
    .json(newTodo);
});

export default router;
